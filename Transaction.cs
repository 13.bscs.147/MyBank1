﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBank
{
    public class Transaction
    {
        public decimal Balance { get; }
        public DateTime Date { get; }
        public string Name { get; }

        public Transaction(string name, DateTime date, decimal balance)
        {
            this.Balance = balance;
            this.Date = date;
            this.Name = name;
        }
    }
}
