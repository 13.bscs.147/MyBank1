﻿using System;

namespace MyBank
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Bank Account properties : NumberId, Name, Balance
            var account1 = new BankAccount("Azwar", 1000);
            Console.WriteLine($"My Name is {account1.Name} and My Account ID is {account1.NumberId} and My Balance is {account1.Balance}");

            account1.MakeDeposit("Azwar", DateTime.Now, 120);



            Console.WriteLine(account1.Balance);

            account1.Withdrawal("Azwar", DateTime.Now, 120);
            Console.WriteLine(account1.Balance);

            account1.GetAccountHistory();

        }
    }
}
