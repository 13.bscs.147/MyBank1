﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBank
{
    public class BankAccount
    {
        // List with properties : Balance, Date, Name
        private List<Transaction> transactionsList = new List<Transaction>();

        public string NumberId { get; }
        public string Name { get; set; }
        public decimal Balance
        {
            get
            {
                decimal balance = 0;
                foreach (var transaction in transactionsList)
                {
                    balance += transaction.Balance;
                }
                return balance;
            }
        }
        private static int accountNumberSeed = 1234567890;

        public BankAccount(string name, decimal balance)
        {
            this.Name = name;
            MakeDeposit(name, DateTime.Now, balance);
            NumberId = accountNumberSeed.ToString();
            accountNumberSeed++;
        }

        public void MakeDeposit(string name, DateTime date, decimal balance)
        {
            var deposit = new Transaction(name, date, balance);
            transactionsList.Add(deposit);
        }

        public void Withdrawal(string name, DateTime date, decimal balance)
        {
            transactionsList.Add(new Transaction(name, date, -balance));
        }
        public void GetAccountHistory()
        {
            var report = new StringBuilder();
            foreach (var transaction in transactionsList)
            {
                Console.WriteLine(report.AppendLine($"{transaction.Name}\t\t{transaction.Date.ToShortDateString()}\t\t{transaction.Balance}"));
            }
        }

    }
}
